# This file is a template, and might need editing before it works on your project.
FROM java:8-jre

COPY initial/target/ /app/
WORKDIR /app/

EXPOSE 9000
CMD ["java", "-jar", "gs-actuator-service-0.1.0.jar"]